# Product Description

App provides user with basic photo viewing, search and sharing functionality. Image search powered by Flickr. 

It consists of three logical parts:
1. Tiled photo view with search option
2. Full screen photo preview with scrolling and zooming capabilities
3. Photo sharing with ability to add caption to image in a "demotivator" style

# Tech Description
App architecture consists of several layers:
1. Networking
2. Mediators
3. Screens

**Networking** layer is presented by these main classes implementing general networking tasks
*NetworkPhotoFlickrService* implements network request logic and response mapping logic. 
*NetworkRequestOffsetPager* provides implementation of stateful pagination logic
*NetworkImageDownloader* handles image downloading and caching

**Mediators** establish opaque connection between UI and business logic
*EntityListLoader* and *EntityListLoaderStrategy* resolve the problem of configurable list loader, serving as high-level wrapper for pager
*CollectionViewSectionManagerAggregate* and *CollectionViewSectionManager* allow decomposing collectionView section logic to loosely coupled entities

**Screens** are generally built using MVVM approach with each screen consisting of viewModel, viewController, section managers and probably action managers
The latter ones are responsible for complex business logic had to be moved outside view controller 

# Future Plans
1. Improve UX of fullscreen preview, including interactive "flick" dismiss transition
2. Add sorting options to the main screen
3. Switch to other image search service that supports GIF images and more qualitative photo content. Google Images, for example
4. Support GIF playback and sharing
5. More image editing options for sharing screen

# Installation notes
Run **pod update** from the command line with current directory set to project root directory 
