//
//  UIViewController+ChildEmbedding.swift
//  PhotoViewer
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Add child view controller to controller's root view
    func embed(childViewController: UIViewController) {
        embed(childViewController: childViewController, toContainerView: view)
    }
    
    /// Add child view controller to target child view
    func embed(childViewController: UIViewController, toContainerView containerView: UIView) {
        assert(containerView === view || containerView.isDescendant(of: view))
        addChildViewController(childViewController)
        childViewController.willMove(toParentViewController: self)
        childViewController.view.frame = containerView.bounds
        childViewController.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, .flexibleHeight]
        containerView.addSubview(childViewController.view)
        childViewController.didMove(toParentViewController: self)
    }
    
    /// Remove child view controller
    func leaveParentViewController() {
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
    }
    
}
