//
//  Dictionary+Lookup.swift
//  PhotoViewer
//
//  Created by VI_Business on 07/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

extension Dictionary where Value: Equatable {
    /// Reverse lookup for dictoinary
    func key(forValue value: Value) -> Key? {
        return first { $0.1 == value }?.0
    }
}
