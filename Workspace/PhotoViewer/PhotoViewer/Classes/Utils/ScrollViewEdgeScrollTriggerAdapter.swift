//
//  ScrollViewBottomScrollTriggerAdapter.swift
//
//  Created by VI_Business on 26/08/2018.
//  Copyright © 2018 supercorp. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

/**
 *  Triggers action when collection view scrolled to bottom
 */
class ScrollViewEdgeScrollTriggerAdapter: NSObject, UIScrollViewDelegate {
    enum ScrollDirection {
        case vertical
        case horizontal
    }
    
    /// Emits when list was scrolled below trigger line
    let edgeScrolledSignal = PublishSubject<Void>()
    
    private static let triggerDistance: CGFloat = 100.0
    private weak var scrollView: UIScrollView!
    private let scrollDirection: ScrollDirection
    private let disposeBag = DisposeBag()
    
    init(scrollView: UIScrollView, scrollDirection: ScrollDirection) {
        self.scrollView = scrollView
        self.scrollDirection = scrollDirection
        super.init()
        
        self.scrollView.rx.didScroll.subscribe(onNext: { [weak self] _ in
            let strongSelf = self!
            // Consider only user-initiated actions
            if !scrollView.isDragging && !scrollView.isTracking && !scrollView.isDecelerating {
                return
            }
            
            if strongSelf.isEdgeTriggered() {
                strongSelf.edgeScrolledSignal.onNext(())
            }
        }).disposed(by: disposeBag)
    }

    private func isVerticalEdgeTriggered() -> Bool {
        let offsetY = scrollView.contentOffset.y
        let maxOffsetY = scrollView.contentSize.height - scrollView.bounds.height + scrollView.contentInset.bottom
        
        return maxOffsetY - offsetY < ScrollViewEdgeScrollTriggerAdapter.triggerDistance
    }
    
    private func isHorizontalEdgeTriggered() -> Bool {
        let offsetX = scrollView.contentOffset.x
        let maxOffsetX = scrollView.contentSize.width - scrollView.bounds.width + scrollView.contentInset.right
        
        return maxOffsetX - offsetX < ScrollViewEdgeScrollTriggerAdapter.triggerDistance
    }
    
    private func isEdgeTriggered() -> Bool {
        switch scrollDirection {
        case .vertical:
            return isVerticalEdgeTriggered()
        case .horizontal:
            return isHorizontalEdgeTriggered()
        }
    }
}
