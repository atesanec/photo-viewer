//
//  UIView+DefaultIdentifiers.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

extension UIView {
    static var defaultReuseId: String {
        return NSStringFromClass(self)
    }
}
