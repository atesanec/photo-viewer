//
//  UIViewController+Topmost.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    /// View controller on the top of visibility stack
    static var topmostViewController: UIViewController {
        guard let window = (UIApplication.shared.delegate as! AppDelegate).window,
        let rootController = window.rootViewController else {
            assert(false)
            return UIViewController()
        }
        
        
        return topmostViewController(withRootController: rootController)
    }
    
    private static func topmostViewController(withRootController root: UIViewController) -> UIViewController {
        if let presentedController = root.presentedViewController {
            return topmostViewController(withRootController: presentedController)
        } else if let navController = root as? UINavigationController {
            return topmostViewController(withRootController: navController.topViewController!)
        }
        
        return root
    }
}
