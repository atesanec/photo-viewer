//
//  PhotoShareViewController.swift
//  PhotoViewer
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import RxKeyboard
import GrowingTextView

/**
 *  Controller for sharing a photo
 */
class PhotoShareViewController: UIViewController {
    private enum ImageSizeControlValue: Int {
        static let controlValueToSizePreset = [
            ImageSizeControlValue.small : NetworkPhotoServiceImageSizePreset.small,
            ImageSizeControlValue.normal: NetworkPhotoServiceImageSizePreset.normal,
            ImageSizeControlValue.original: NetworkPhotoServiceImageSizePreset.original
            ]
        
        case small = 0
        case normal = 1
        case original = 2

        func toImageSizePreset() -> NetworkPhotoServiceImageSizePreset {
            return ImageSizeControlValue.controlValueToSizePreset[self]!
        }
        
        init?(preset: NetworkPhotoServiceImageSizePreset) {
            self.init(rawValue: ImageSizeControlValue.controlValueToSizePreset.key(forValue: preset)!.rawValue)
        }
    }
    
    @IBOutlet weak var imageSizeControl: UISegmentedControl!
    @IBOutlet weak var imageCaptionField: GrowingTextView!
    @IBOutlet weak var imageCaptionLabel: UILabel!
    @IBOutlet weak var photoPreviewImageView: UIImageView!
    @IBOutlet weak var scrollVew: UIScrollView!
    
    private var viewModel: PhotoShareViewModel!
    private var actionManager: PhotoShareActionManager!
    private let disposeBag = DisposeBag()
    private static let imageLoadRetryTimeInterval = 2.0
    private static let imageCaptionFieldMinHeight: CGFloat = 54.0
    
    convenience init(photoFeedItem: NetworkPhotoServicePhotoFeedItem) {
        self.init(nibName: "PhotoShareViewController", bundle: nil)
        
        self.viewModel = PhotoShareViewModel(photoFeedItem: photoFeedItem)
        self.actionManager = PhotoShareActionManager(viewModel: viewModel)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        configureUI()
        setupUIObservations()
        setupKeyboardObservations()
    }
    
    private func configureUI() {
        self.title = "SharePhoto".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
        
        imageCaptionField.placeholder = "ImageCaption".localized
        imageCaptionField.minHeight = PhotoShareViewController.imageCaptionFieldMinHeight
        imageCaptionField.layer.borderWidth = 1
        imageCaptionField.layer.cornerRadius = 4;
        imageCaptionField.layer.borderColor = UIColor.lightGray.cgColor
        
        imageSizeControl.selectedSegmentIndex = ImageSizeControlValue.original.rawValue
        imageCaptionField.text = viewModel.photoFeedItem.title
        
        imageCaptionLabel.text = "ImageCaption".localized
        
        let previewImage = viewModel.photoFeedItem.imageWithSize(preset: .small)
        // Retry uploading image in case of error
        RootServicesDomain.sharedInstance.imageDownloader.downloadImage(url: previewImage.imageURL).retryWhen({ _ in
            return Observable<Int64>.timer(RxTimeInterval(PhotoShareViewController.imageLoadRetryTimeInterval), scheduler: MainScheduler.instance)
        }).bind(to: photoPreviewImageView.rx.image).disposed(by: disposeBag)
    }
    
    private func setupUIObservations() {
        imageCaptionField.rx.text.ifEmpty(default: "")
            .map {$0!.trimmingCharacters(in: .whitespacesAndNewlines)}
            .distinctUntilChanged()
            .bind(to: viewModel.caption)
            .disposed(by: disposeBag)
        
        imageSizeControl.rx.value.map {ImageSizeControlValue(rawValue: $0)!.toImageSizePreset()}
            .bind(to: viewModel.exportSizePreset)
            .disposed(by: disposeBag)
        
        self.navigationItem.rightBarButtonItem!.rx.tap.subscribe { [weak self] _ in
            self!.actionManager.sharePhoto()
        }.disposed(by: disposeBag)
    }
    
    private func setupKeyboardObservations() {
        RxKeyboard.instance.visibleHeight.drive(onNext: {[weak self] height in
            let strongSelf = self!
            var contentInset = strongSelf.scrollVew.contentInset
            contentInset.bottom = height
            strongSelf.scrollVew.contentInset = contentInset
            strongSelf.scrollVew.scrollIndicatorInsets = contentInset
        }).disposed(by: disposeBag)
    }
}
