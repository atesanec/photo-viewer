//
//  PhotoShareActionManager.swift
//  PhotoViewer
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Handles complex business logic for photo sharing screen
 */
class PhotoShareActionManager {
    private let viewModel: PhotoShareViewModel
    private let imageProcessor = PhotoProcessor()
    private let disposeBag = DisposeBag()
    
    init(viewModel: PhotoShareViewModel) {
        self.viewModel = viewModel
    }
    
    func sharePhoto() {
        let imageURL = viewModel.photoFeedItem.imageWithSize(preset: try! viewModel.exportSizePreset.value()).imageURL
        let caption = try! viewModel.caption.value()
        /// Download, process image and then present sharing menu
        RootServicesDomain.sharedInstance.imageDownloader.downloadImage(url: imageURL)
            .flatMap { [weak self] image in
                return self!.imageProcessor.makeCaptionedImage(image: image!, caption: caption)
            }.observeOn(MainScheduler.instance)
            .flatMap {[weak self] in self!.presentSharingMenu(forImage: $0)}
            .subscribe(onError: { (error) in
                RootServicesDomain.sharedInstance.alertMessagePresenter.presentAlert(title: "ImageShareError".localized, text: nil)
            }, onCompleted: {
                UIViewController.topmostViewController.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
    }
    
    private func presentSharingMenu(forImage image: UIImage) -> Observable<Void> {
        return Observable<Void>.create({ observer -> Disposable in
            let controller = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            controller.completionWithItemsHandler = {(_, _, _, error) in
                if let shareError = error {
                    observer.onError(shareError)
                } else {
                    observer.onCompleted()
                }
            }
            
            UIViewController.topmostViewController.present(controller, animated: true, completion: nil)
            return Disposables.create()
        })
    }
}
