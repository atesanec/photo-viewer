//
//  PhotoShareViewModel.swift
//  PhotoViewer
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  View model for photo sharing screen
 */
class PhotoShareViewModel {
    /// Photo feed item for sharing
    let photoFeedItem: NetworkPhotoServicePhotoFeedItem
    /// Selected image caption
    let caption = BehaviorSubject<String>(value: "")
    /// Selected image export preset
    let exportSizePreset = BehaviorSubject<NetworkPhotoServiceImageSizePreset>(value: .original)
    
    init(photoFeedItem: NetworkPhotoServicePhotoFeedItem) {
        self.photoFeedItem = photoFeedItem
    }
}
