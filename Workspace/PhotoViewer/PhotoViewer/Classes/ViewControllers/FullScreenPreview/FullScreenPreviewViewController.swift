//
//  FullScreenPreviewViewController.swift
//  PhotoViewer
//
//  Created by VI_Business on 05/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 * Full screen photo preview
 */
class FullScreenPreviewViewController: UIViewController {
    enum SectionIndex: Int {
        case listItems
    }
    
    struct Configuration {
        /// Loader for photo feed items
        let photoFeedLoader: EntityListLoader<NetworkPhotoServicePhotoFeedItem>
        /// Photo feed item id to focus on by default
        let startPhotoId: String
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var shareButton: UIButton!
    
    private var sectionManagerAggregate: CollectionViewSectionManagerAggregate!
    private var viewModel: FullScreenPreviewViewModel!
    private var thumbsListLoader: EntityListLoader<NetworkPhotoServicePhotoFeedItem>!
    private var edgeScrollTriggerAdapter: ScrollViewEdgeScrollTriggerAdapter!
    private let disposeBag = DisposeBag()
    
    convenience init(config: Configuration) {
        self.init(nibName: "FullScreenPreviewViewController", bundle: nil)
        self.thumbsListLoader = config.photoFeedLoader
        
        self.viewModel = FullScreenPreviewViewModel(startPhotoId: config.startPhotoId)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation{
        return .slide
    }
    override var prefersStatusBarHidden: Bool {
        return shareButton == nil ? false : shareButton.alpha == 0
    }
    
    override func viewDidLoad() {
        collectionView.contentInsetAdjustmentBehavior = .never
        
        thumbsListLoader.loadedEntities.bind(to: viewModel.loadedEntities).disposed(by: disposeBag)
        thumbsListLoader.isLoading.bind(to: viewModel.isLoading).disposed(by: disposeBag)
        
        let listSectionManager = FullScreenPreviewItemSectionManager(viewModel: viewModel)
        sectionManagerAggregate = CollectionViewSectionManagerAggregate(sectionManagers: [listSectionManager], collectionView: collectionView)
        
        edgeScrollTriggerAdapter = ScrollViewEdgeScrollTriggerAdapter(scrollView: collectionView, scrollDirection: .horizontal)
        
        setupModelObservations()
        setupUIObservations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController!.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController!.navigationBar.isTranslucent = false
        updateBottombarsVisibility(visible: true)
    }
    
    // MARK: - Observations
    
    private func setupModelObservations() {
        edgeScrollTriggerAdapter.edgeScrolledSignal.subscribe(onNext: { [weak self] _ in
            self!.thumbsListLoader.loadNextBatch()
        }).disposed(by: disposeBag)
        
        thumbsListLoader.configurationChangedSignal.subscribe(onNext: { [weak self] () in
            self!.thumbsListLoader.loadNextBatch()
        }).disposed(by: disposeBag)
        
        viewModel.visiblePhotoId.distinctUntilChanged().subscribe(onNext: { [weak self] photoId in
            let strongSelf = self!
            let items = try! strongSelf.viewModel.loadedEntities.value()
            let item = items.first(where: {$0.photoId == photoId})
            if let foundItem = item {
                strongSelf.title = foundItem.title
            } else {
                strongSelf.title = nil
            }
        }).disposed(by: disposeBag)
        
        viewModel.toggleEdgePanelsVisibility.subscribe(onNext: { [weak self] (_) in
            let strongSelf = self!
            let isHidden = strongSelf.navigationController!.isNavigationBarHidden
            strongSelf.updateBottombarsVisibility(visible: isHidden)
        }).disposed(by: disposeBag)
    }
    
    private func setupUIObservations() {
        let startId = viewModel.startPhotoId
        // Scroll to start item id once after scroll view has updated its data
        self.rx.sentMessage(#selector(UIViewController.viewDidLayoutSubviews)).take(1).map({_ in startId})
            .subscribe(onNext: { [weak self] photoId in
                let strongSelf = self!
                let items = try! strongSelf.viewModel.loadedEntities.value()
                let itemIndex = items.index(where: {$0.photoId == photoId})
                if let foundIndex = itemIndex {
                    strongSelf.collectionView.scrollToItem(at: IndexPath(row: foundIndex, section: SectionIndex.listItems.rawValue),
                                                           at: .centeredHorizontally,
                                                           animated: false)
                    strongSelf.viewModel.visiblePhotoId.onNext(startId)
                }
            }).disposed(by: disposeBag)
        
        collectionView.rx.didScroll.map({[weak self] _ -> String? in
            let strongSelf = self!
            
            if strongSelf.collectionView.contentSize.width == 0 {
                return nil
            }
            
            let items = try! strongSelf.viewModel.loadedEntities.value()
            let offsetX = strongSelf.collectionView.contentOffset.x
            let itemIndex = Int(floor(offsetX / strongSelf.collectionView.bounds.width))
            return items[itemIndex].photoId
        }).bind(to: viewModel.visiblePhotoId).disposed(by: disposeBag)
        
        shareButton.rx.tap.subscribe { [weak self] _ in
            let strongSelf = self!
            let photoId = try! strongSelf.viewModel.visiblePhotoId.value()!
            let controller = PhotoShareViewController(photoFeedItem: strongSelf.photoFeedItem(withId: photoId)!)
            UIViewController.topmostViewController.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
        }.disposed(by: disposeBag)
    }
    
    private func photoFeedItem(withId: String) -> NetworkPhotoServicePhotoFeedItem? {
        let items = try! viewModel.loadedEntities.value()
        let photoId = try! viewModel.visiblePhotoId.value()!
        return items.first(where: {$0.photoId == photoId})
    }
    
    // MARK: - UI
    
    private func updateBottombarsVisibility(visible: Bool) {
        navigationController?.setNavigationBarHidden(!visible, animated: true)
        UIView.animate(withDuration: 0.2, animations: {self.shareButton.alpha = visible ? 1.0 : 0.0})
        setNeedsStatusBarAppearanceUpdate()
    }
}
