//
//  FullScreenPreviewItemSectionManager.swift
//  PhotoViewer
//
//  Created by VI_Business on 05/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Section manager for the list of photo previews
 */
class FullScreenPreviewItemSectionManager: CollectionViewSectionManager {
    var collectionView: UICollectionView!
    var sectionIndex: Int
    var dataObservable =  BehaviorSubject<[Any]>(value: [])
    
    private let disposeBag = DisposeBag()
    private let viewModel: FullScreenPreviewViewModel
    
    init(viewModel: FullScreenPreviewViewModel) {
        self.sectionIndex = FullScreenPreviewViewController.SectionIndex.listItems.rawValue
        self.viewModel = viewModel
        
        viewModel.loadedEntities.flatMap({ (items) in
            return Observable<[Any]>.just(items)
        }).bind(to: dataObservable).disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(FullScreenPreviewItemCell.self, forCellWithReuseIdentifier: FullScreenPreviewItemCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
        let feedItem = try! self.dataObservable.value()[atIndex] as! NetworkPhotoServicePhotoFeedItem
        let imageURL = feedItem.imageWithSize(preset: .normal).imageURL
        
        let driver = RootServicesDomain.sharedInstance.imageDownloader.downloadImage(url: imageURL).asDriver(onErrorJustReturn: nil)
        let config = FullScreenPreviewItemCell.Configuration(image: driver.asObservable(), singleTapObserver: viewModel.toggleEdgePanelsVisibility)
        
        let photoCell = cell as! FullScreenPreviewItemCell
        photoCell.configure(info: config)
    }
    
    func cellId(atIndex: Int) -> String {
        return FullScreenPreviewItemCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        return collectionView.bounds.size
    }
    
    func itemSelected(atIndex: Int) {
        // Do nothing
    }
}
