//
//  FullScreenPreviewItemCell.swift
//  PhotoViewer
//
//  Created by VI_Business on 05/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import RxGesture

/**
 *  Cell with zoomable image view
 */
class FullScreenPreviewItemCell: UICollectionViewCell, UIScrollViewDelegate {
    struct Configuration {
        let image: Observable<UIImage?>
        let singleTapObserver: PublishSubject<Void>
    }
    
    private let imageView = UIImageView()
    private let zoomScrollView = UIScrollView()
    private var tapGestureObservable: Observable<UITapGestureRecognizer>!
    private var disposeBag = DisposeBag()
    private var reuseDisposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.backgroundColor = UIColor.clear
        
        contentView.addSubview(zoomScrollView)
        
        tapGestureObservable = zoomScrollView.rx
            .tapGesture(numberOfTouchesRequired: 1, numberOfTapsRequired: 1, configuration: { (_, delegate) in
                delegate.otherFailureRequirementPolicy = GestureRecognizerDelegatePolicy<(GestureRecognizer, GestureRecognizer)>
                    .custom({ (source, other) -> Bool in
                        return other is UITapGestureRecognizer
                })
            }).when(.recognized)
        
        imageView.rx.tapGesture(numberOfTouchesRequired: 1, numberOfTapsRequired: 2, configuration: {_,_ in})
            .when(.recognized)
            .subscribe(onNext: { [weak self] (gesture) in
                self!.doubleTapHandler(location: gesture.location(in: gesture.view))
            }).disposed(by: disposeBag)
        
        zoomScrollView.contentInsetAdjustmentBehavior = .never
        zoomScrollView.showsVerticalScrollIndicator = false
        zoomScrollView.showsHorizontalScrollIndicator = false
        zoomScrollView.delegate = self
        zoomScrollView.addSubview(imageView)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func configure(info: Configuration) {
        imageView.image = nil
        info.image.subscribe(onNext: { [weak self] (image) in
            let strongSelf = self!
            
            if let zoomImage = image {
                strongSelf.imageView.image = zoomImage
                strongSelf.imageView.sizeToFit()
                strongSelf.zoomScrollView.contentSize = zoomImage.size
                strongSelf.resetScrollViewZoom()
                strongSelf.alignImageView()
            }
        }).disposed(by: reuseDisposeBag)
        
        tapGestureObservable.map{_ in ()}.bind(to: info.singleTapObserver).disposed(by: reuseDisposeBag)
    }
    
    private func doubleTapHandler(location: CGPoint) {
        var zoom = zoomScrollView.zoomScale
        let minZoom = zoomScrollView.minimumZoomScale
        let maxZoom = zoomScrollView.maximumZoomScale
        if zoom > minZoom + 0.5 * (maxZoom - minZoom) {
            zoom = minZoom
        } else {
            zoom = maxZoom
        }
        
        zoomScrollView.setZoomScale(zoom, animated: true)
    }
    
    override func prepareForReuse() {
        reuseDisposeBag = DisposeBag()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        zoomScrollView.frame = contentView.bounds
    }
    
    // MARK: - Private functions
    
    private func resetScrollViewZoom() {
        let imageSize = imageView.image!.size
        let bounds = zoomScrollView.bounds
        
        let minScale = min(bounds.width / imageSize.width, bounds.height / imageSize.height)
        
        zoomScrollView.minimumZoomScale = min(minScale, 1.0)
        zoomScrollView.maximumZoomScale = 1.0
        zoomScrollView.zoomScale = zoomScrollView.minimumZoomScale
    }
    
    /// Center image view within scroll view
    private func alignImageView() {
        var frameToCenter = imageView.frame
        
        if frameToCenter.size.width < bounds.width {
            frameToCenter.origin.x = (bounds.width - frameToCenter.size.width) / 2
        }
        else {
            frameToCenter.origin.x = 0
        }
        
        if frameToCenter.size.height < bounds.height {
            frameToCenter.origin.y = (bounds.height - frameToCenter.size.height) / 2
        }
        else {
            frameToCenter.origin.y = 0
        }
        
        imageView.frame = frameToCenter
    }
    
    // MARK: - ScrollViewDelegate methods
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        alignImageView()
    }
}
