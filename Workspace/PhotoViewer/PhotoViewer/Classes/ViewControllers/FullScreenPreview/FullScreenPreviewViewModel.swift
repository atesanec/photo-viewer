//
//  FullScreenPreviewViewModel.swift
//  PhotoViewer
//
//  Created by VI_Business on 05/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Full screen preview view model
 */
class FullScreenPreviewViewModel {
    /// Photo id to focus on by default
    let startPhotoId: String
    /// All loaded entities
    let loadedEntities = BehaviorSubject<[NetworkPhotoServicePhotoFeedItem]>(value: [NetworkPhotoServicePhotoFeedItem]())
    /// Is list loading
    let isLoading = BehaviorSubject<Bool>(value: false)
    /// Photo feed item id for currently visible photo
    let visiblePhotoId = BehaviorSubject<String?>(value: nil)
    /// Signal to change visibility of all controls obscuring photo preview
    let toggleEdgePanelsVisibility = PublishSubject<Void>()
    
    init(startPhotoId: String) {
        self.startPhotoId = startPhotoId
    }
}
