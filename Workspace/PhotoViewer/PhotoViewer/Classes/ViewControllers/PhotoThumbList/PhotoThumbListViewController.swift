//
//  PhotoThumbListViewController.swift
//  PhotoViewer
//
//  Created by VI_Business on 02/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

/**
 *  List of small photo thumbs
 */
class PhotoThumbListViewController: UIViewController {
    enum SectionIndex: Int {
        case listItems
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var sectionManagerAggregate: CollectionViewSectionManagerAggregate! = nil
    private let viewModel = PhotoThumbListViewModel()
    private var thumbsListLoader: EntityListLoader<NetworkPhotoServicePhotoFeedItem>! = nil
    private let searchController = UISearchController(searchResultsController: nil)
    private var edgeScrollTriggerAdapter: ScrollViewEdgeScrollTriggerAdapter! = nil
    private var listPlaceholderController: LoadedListPlaceholderViewController! = nil
    
    private let disposeBag = DisposeBag()
    
    private static let searchQueryUpdateDelay = 2.0

    convenience init() {
        self.init(nibName: "PhotoThumbListViewController", bundle: nil)
    }
    
    override func viewDidLoad() {
        configureListLoader()
        configureUI()
        setupModelObservations()
        setupUIObservations()
        
        thumbsListLoader.loadNextBatch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isTranslucent = false
    }
    
    /// MARK: - Configuration
    
    private func configureListLoader() {
        thumbsListLoader = EntityListLoader(strategy: PhotoThumbListLoaderStrategy(viewModel: viewModel))
        
        let listSectionManager = PhotoThumbListItemSectionManager(viewModel: viewModel)
        sectionManagerAggregate = CollectionViewSectionManagerAggregate(sectionManagers: [listSectionManager], collectionView: collectionView)
        
        edgeScrollTriggerAdapter = ScrollViewEdgeScrollTriggerAdapter(scrollView: collectionView, scrollDirection: .vertical)
    }
    
    // MARK: - Observations
    
    private func configureUI() {
        self.title = "ThumbsListTitle".localized
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = PhotoThumbListItemSectionManager.thumbItemsSpacing
        layout.minimumLineSpacing = PhotoThumbListItemSectionManager.thumbItemsSpacing
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "ThumbSearchPlaceholder".localized
        navigationItem.searchController = searchController
        
        listPlaceholderController = LoadedListPlaceholderViewController()
        self.embed(childViewController: listPlaceholderController)
        listPlaceholderController.view.frame = self.view.bounds
        listPlaceholderController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    private func setupModelObservations() {
        edgeScrollTriggerAdapter.edgeScrolledSignal.subscribe(onNext: { [weak self] _ in
            self!.thumbsListLoader.loadNextBatch()
        }).disposed(by: disposeBag)
        
        thumbsListLoader.configurationChangedSignal.subscribe(onNext: { [weak self] () in
            self!.thumbsListLoader.loadNextBatch()
        }).disposed(by: disposeBag)
        
        viewModel.selectedThumbItemId.subscribe(onNext: { [weak self] photoId in
            let strongSelf = self!
            let config = FullScreenPreviewViewController.Configuration(photoFeedLoader: strongSelf.thumbsListLoader, startPhotoId: photoId)
            let controller = FullScreenPreviewViewController(config: config)
            strongSelf.navigationController?.pushViewController(controller, animated: true)
        }).disposed(by: disposeBag)
        
        viewModel.loadedEntities.map({$0.count}).bind(to: listPlaceholderController.itemCountObserver).disposed(by: disposeBag)
        viewModel.isLoading.bind(to: listPlaceholderController.isLoadingObserver).disposed(by: disposeBag)
        
        thumbsListLoader.loadedEntities.bind(to: viewModel.loadedEntities).disposed(by: disposeBag)
        thumbsListLoader.isLoading.bind(to: viewModel.isLoading).disposed(by: disposeBag)
    }
    
    private func setupUIObservations() {
        // Start search some time after user has stopped editing textField
        searchController.searchBar.rx.text.ifEmpty(default: "")
            .debounce(PhotoThumbListViewController.searchQueryUpdateDelay, scheduler: MainScheduler.instance)
            .map({$0!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)})
            .distinctUntilChanged()
            .bind(to: viewModel.searchQuery)
            .disposed(by: disposeBag)
        
        listPlaceholderController.reloadButtonTappedObservable.subscribe(onNext: { [weak self] _ in
            self!.thumbsListLoader.loadNextBatch()
        }).disposed(by: disposeBag)
    }
    
    
}
