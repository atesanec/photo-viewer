//
//  PhotoThumbListViewModel.swift
//  PhotoViewer
//
//  Created by VI_Business on 02/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift

/**
 *  View model for photo thumbs list
 */
class PhotoThumbListViewModel {
    /// All loaded entities
    let loadedEntities = BehaviorSubject<[NetworkPhotoServicePhotoFeedItem]>(value: [NetworkPhotoServicePhotoFeedItem]())
    /// Is entity list loading now
    let isLoading = BehaviorSubject<Bool>(value: false)
    /// Current photo search query
    let searchQuery = BehaviorSubject<String>(value: "")
    /// Photo feed item has been selected
    let selectedThumbItemId = PublishSubject<String>()
}
