//
//  PhotoThumbListPhotosSectionManager.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift

class PhotoThumbListItemSectionManager: CollectionViewSectionManager {
    var collectionView: UICollectionView!
    var sectionIndex: Int
    var dataObservable =  BehaviorSubject<[Any]>(value: [])
    
    static let thumbItemsSpacing: CGFloat = 2.0
    private static let thumbsPerRowCount = 4
    private let disposeBag = DisposeBag()
    private let viewModel: PhotoThumbListViewModel
    
    init(viewModel: PhotoThumbListViewModel) {
        self.sectionIndex = PhotoThumbListViewController.SectionIndex.listItems.rawValue
        self.viewModel = viewModel
        
        viewModel.loadedEntities.flatMap({ (items) in
            return Observable<[Any]>.just(items)
        }).bind(to: dataObservable).disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(PhotoThumbListCell.self, forCellWithReuseIdentifier: PhotoThumbListCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
        let feedItem = try! self.dataObservable.value()[atIndex] as! NetworkPhotoServicePhotoFeedItem
        let imageURL = feedItem.imageWithSize(preset: .thumbnail).imageURL
        
        let driver = RootServicesDomain.sharedInstance.imageDownloader.downloadImage(url: imageURL).asDriver(onErrorJustReturn: nil)
        let config = PhotoThumbListCell.Configuration(image: driver.asObservable())
        
        let thumbCell = cell as! PhotoThumbListCell
        thumbCell.configure(info: config)
    }
    
    func cellId(atIndex: Int) -> String {
        return PhotoThumbListCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        let itemsInRow = PhotoThumbListItemSectionManager.thumbsPerRowCount
        let availableWidth = collectionView.bounds.width - PhotoThumbListItemSectionManager.thumbItemsSpacing * CGFloat(itemsInRow - 1)
        let itemSize = availableWidth / CGFloat(itemsInRow)
        return CGSize.init(width: itemSize, height: itemSize)
    }
    
    func itemSelected(atIndex: Int) {
        let feedItem = try! self.dataObservable.value()[atIndex] as! NetworkPhotoServicePhotoFeedItem
        viewModel.selectedThumbItemId.onNext(feedItem.photoId)
    }
}
