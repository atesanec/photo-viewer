//
//  PhotoThumbListLoaderStrategy.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Loader strategy for thumbs list
 */
class PhotoThumbListLoaderStrategy: EntityListLoaderStrategy<NetworkPhotoServicePhotoFeedItem> {
    private let viewModel: PhotoThumbListViewModel
    private let disposeBag = DisposeBag()
    private let batchSize = 40
    
    init(viewModel: PhotoThumbListViewModel) {
        self.viewModel = viewModel
        super.init()
        
        setupObservations()
    }
    
    private func setupObservations() {
        viewModel.searchQuery.asObserver().distinctUntilChanged().subscribe(onNext: {[weak self] (query) in
            let strongSelf = self!
            strongSelf.pager = RootServicesDomain.sharedInstance.networkPhotoService.photoFeedPager(batchSize: strongSelf.batchSize, query: query)
            strongSelf.pagerInvalidatedSignal.onNext(())
        }).disposed(by: disposeBag)
    }
}
