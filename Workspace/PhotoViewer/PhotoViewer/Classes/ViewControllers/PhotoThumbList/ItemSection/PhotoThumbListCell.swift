//
//  PhotoThumbListCell.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Cell containing a thumb of a photo
 */
class PhotoThumbListCell: UICollectionViewCell {
    struct Configuration {
        let image: Observable<UIImage?>
    }
    
    private let imageView = UIImageView()
    private var disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.backgroundColor = UIColor.lightGray
        
        contentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }
    
    func configure(info: Configuration) {
        imageView.image = nil
        info.image.bind(to: imageView.rx.image).disposed(by: disposeBag)
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = contentView.bounds
    }
}
