//
//  AppDelegate.swift
//  PhotoViewer
//
//  Created by VI_Business on 01/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let rootController = UINavigationController(rootViewController: PhotoThumbListViewController())
        
        self.window = UIWindow()
        self.window!.rootViewController = rootController
        self.window!.makeKeyAndVisible()
        
        return true
    }
}

