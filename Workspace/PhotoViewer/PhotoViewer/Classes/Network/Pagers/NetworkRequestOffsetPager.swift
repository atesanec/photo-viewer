//
//  NetworkRequestPager.swift
//  BerlinTourReviews
//
//  Created by VI_Business on 01/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

typealias NetworkRequestOffsetPagerBatch<T> = (entities: [T], allLoaded: Bool)

/**
 *  Pager configuration
 */
struct NetworkRequestOffsetPagerConfiguration<T> {
    /// Request parameters which are common for all batch rquests
    let requestParameters : [String: Any]
    /// Page number parameter name
    let pagingParamName: String?
    /// Item count in a batch parameter name
    let countParamName: String?
    
    /// Item count in a batch
    let batchSize: Int
    
    /// Maps a corresponding element of response into item
    let mappingHandler: (Any) -> T
    /// Mapping queue
    let mappingQueue: DispatchQueue
    /// Maps data to collection of raw item elements. Each element then is being passed to *mappingHandler*
    let responseToCollectionTransformer: (Data) -> Array<Any>
    
    /// Paged request base URL
    let requestURL: URL
}

/**
 *  Stateful pager for loading batched request.
 *  Each batch is parametrized by two arguments - current page and number of elements in the returned batch
 */
class NetworkRequestOffsetPager<T> {
    enum DefaultParamNames: String {
        case count
        case page
    }
    
    /// All loaded items
    let loadedEntities = BehaviorSubject<[T]>(value: [T]())
    /// Are all items loaded
    let allLoaded = BehaviorSubject<Bool>(value: false)
    /// Are items being loaded
    let isLoading = BehaviorSubject<Bool>(value: false)
    
    private let configuration: NetworkRequestOffsetPagerConfiguration<T>
    
    init(configuration: NetworkRequestOffsetPagerConfiguration<T>) {
        self.configuration = configuration
    }
    
    /// Load net batch of items
    func loadNext() -> Observable<NetworkRequestOffsetPagerBatch<T>> {
        return Observable.create { [weak self] observer -> Disposable in
            let strongSelf = self!
            let path = strongSelf.configuration.requestURL
            let params = strongSelf.loadNextRequestParams()
            
            strongSelf.isLoading.onNext(true)
            let task = Alamofire.request(path, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
                .responseData(queue: strongSelf.configuration.mappingQueue) { response in
                    switch(response.result) {
                    case .success(_):
                        let rawResponseItems = strongSelf.configuration.responseToCollectionTransformer(response.value!)
                        let batch = strongSelf.generateBatch(fromResponseItems:rawResponseItems)
                        observer.onNext(batch)
                        
                        var loadedEntities = try! strongSelf.loadedEntities.value()
                        loadedEntities.append(contentsOf: batch.entities)
                        strongSelf.loadedEntities.onNext(loadedEntities)
                        strongSelf.allLoaded.onNext(batch.allLoaded)
                        
                        observer.onCompleted()
                        break
                        
                    case .failure(_):
                        observer.onError(response.result.error!)
                        break
                    }
                    
                    strongSelf.isLoading.onNext(false)
            }
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    /// Reset pager to initial state
    func reset() {
        loadedEntities.onNext([T]())
        allLoaded.onNext(false)
    }
    
    // MARK: - Private functions
        
    private func loadNextRequestParams() -> [String: Any] {
        let staticParams = configuration.requestParameters
        let countParamName = configuration.countParamName ?? DefaultParamNames.count.rawValue
        let count = configuration.batchSize
        
        let pageParamName = configuration.pagingParamName ?? DefaultParamNames.page.rawValue
        let currentPage = try! 1 + Int(ceil(Double(loadedEntities.value().count / count)))
        
        var result = staticParams
        result[countParamName] = count
        result[pageParamName] = currentPage
        return result
    }
    
    private func generateBatch(fromResponseItems: Array<Any>) -> NetworkRequestOffsetPagerBatch<T> {
        var objects = [T]()
        for item in fromResponseItems {
            objects.append(configuration.mappingHandler(item))
        }
        
        let allLoaded = objects.count < configuration.batchSize
        
        return (entities: objects, allLoaded: allLoaded)
    }
}
