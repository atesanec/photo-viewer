//
//  PhotoNetworkService.swift
//  PhotoViewer
//
//  Created by VI_Business on 01/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift
/**
 * Image size presets
 */
enum NetworkPhotoServiceImageSizePreset: Int {
    case thumbnail = 0
    case small
    case normal
    case original
}

/**
 * Protocol for abstract network service
 */
protocol NetworkPhotoService {
    /// Pager for loading photo feed item batches
    func photoFeedPager(batchSize: Int, query: String?) -> NetworkRequestOffsetPager<NetworkPhotoServicePhotoFeedItem>
}
