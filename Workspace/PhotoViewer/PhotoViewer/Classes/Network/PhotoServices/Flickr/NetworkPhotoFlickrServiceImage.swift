//
//  NetworkPhotoFlickrServiceImage.swift
//  PhotoViewer
//
//  Created by VI_Business on 02/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import SWXMLHash

class NetworkPhotoFlickrServiceImage: NetworkPhotoServiceImage {
    let imageURL: URL
    let imageSize: CGSize
    
    init?(response: XMLIndexer, imageSuffix: String) {
        let attrs = response.element!.allAttributes
        guard let urlAttr = attrs["url_" + imageSuffix]?.text,
            let imageURL = URL(string: urlAttr) else {
                return nil
        }
        
        self.imageURL = imageURL
        let width = Int(attrs["width_" + imageSuffix]!.text)!
        let height = Int(attrs["height_" + imageSuffix]!.text)!
        self.imageSize = CGSize(width: width, height: height)
    }
}
