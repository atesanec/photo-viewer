//
//  FlickrNetworkPhotoService.swift
//  PhotoViewer
//
//  Created by VI_Business on 01/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import SWXMLHash

class NetworkPhotoFlickrService : NetworkPhotoService {
    enum Params: String {
        case apiKey = "api_key"
        case searchQuery = "text"
        case method
        case page
        case batchSize = "per_page"
        case extraResponseFields = "extras"
        case sortBy = "sort"
    }
    
    enum Methods: String {
        case photoSearch = "flickr.photos.search"
        case recentPhotos = "flickr.photos.getRecent"
    }
    
    enum SortType: String {
        case popularity = "interestingness-desc"
        case relevance
    }
    
    private let mappingQueue = DispatchQueue(label: "com.coolcorp.networkPhotoFlickrService")
    
    private static let baseURL = URL(string: "https://api.flickr.com/services/rest")!
    private static let apiKey = "a24fd5bd5dbf2706557ab62918b90e64"
    private static let photoFeedItemResponseFields = [
        "url_t",
        "width_t",
        "height_t",
        "url_m",
        "width_m",
        "height_m",
        "url_l",
        "width_l",
        "height_l",
        "url_o",
        "width_o",
        "height_o",
    ]
    
    func photoFeedPager(batchSize: Int, query: String?) -> NetworkRequestOffsetPager<NetworkPhotoServicePhotoFeedItem> {
        let isEmptyQuery = query == nil || query!.isEmpty
        var params: [String: Any] = [
            Params.method.rawValue : isEmptyQuery ? Methods.recentPhotos.rawValue : Methods.photoSearch.rawValue,
            Params.apiKey.rawValue : NetworkPhotoFlickrService.apiKey,
            Params.extraResponseFields.rawValue: NetworkPhotoFlickrService.photoFeedItemResponseFields.joined(separator: ","),
            Params.sortBy.rawValue: SortType.relevance.rawValue
        ]
        
        if !isEmptyQuery {
            params[Params.searchQuery.rawValue] = query!
        }
        
        let path = NetworkPhotoFlickrService.baseURL
        let config = NetworkRequestOffsetPagerConfiguration<NetworkPhotoServicePhotoFeedItem>(requestParameters: params,
                                                            pagingParamName: Params.page.rawValue,
                                                            countParamName: Params.batchSize.rawValue,
                                                            batchSize: batchSize,
                                                            mappingHandler: { (item: Any) -> NetworkPhotoFlickrServicePhotoFeedItem in
                                                                let x = NetworkPhotoFlickrServicePhotoFeedItem(response: item as! XMLIndexer)
                                                                return x
                                                                
        },
                                                            mappingQueue: mappingQueue,
                                                            responseToCollectionTransformer: {SWXMLHash.parse($0)["rsp"]["photos"].children},
                                                            requestURL: path)
        return NetworkRequestOffsetPager<NetworkPhotoServicePhotoFeedItem>(configuration: config)
    }
}
