//
//  NetworkPhotoFlickrPhotoFeedItem.swift
//  PhotoViewer
//
//  Created by VI_Business on 01/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import SWXMLHash

class NetworkPhotoFlickrServicePhotoFeedItem: NetworkPhotoServicePhotoFeedItem {
    let photoId: String
    let title: String
    private var images = [NetworkPhotoServiceImageSizePreset: NetworkPhotoFlickrServiceImage]()
    
    init(response: XMLIndexer) {
        let element = response.element!
        self.photoId = element.attribute(by: "id")!.text
        self.title = element.attribute(by: "title")!.text
        
        images[NetworkPhotoServiceImageSizePreset.thumbnail] = NetworkPhotoFlickrServiceImage(response: response, imageSuffix: "t")
        images[NetworkPhotoServiceImageSizePreset.small] = NetworkPhotoFlickrServiceImage(response: response, imageSuffix: "m")
        images[NetworkPhotoServiceImageSizePreset.normal] = NetworkPhotoFlickrServiceImage(response: response, imageSuffix: "l")
        images[NetworkPhotoServiceImageSizePreset.original] = NetworkPhotoFlickrServiceImage(response: response, imageSuffix: "o")
    }
    
    func imageWithSize(preset: NetworkPhotoServiceImageSizePreset) -> NetworkPhotoServiceImage {
        // Some image formats may be missing, while thumbnail should be always present
        for rawPreset in (NetworkPhotoServiceImageSizePreset.thumbnail.rawValue...preset.rawValue).reversed() {
            let preset = NetworkPhotoServiceImageSizePreset(rawValue: rawPreset)!
            if let image = images[preset] {
                return image
            }
        }
        
        return images[NetworkPhotoServiceImageSizePreset.thumbnail]!
    }
}
