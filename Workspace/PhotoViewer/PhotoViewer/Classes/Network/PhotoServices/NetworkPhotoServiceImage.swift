//
//  NetworkPhotoServiceImage.swift
//  PhotoViewer
//
//  Created by VI_Business on 02/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import UIKit

/**
 * Protocol for image returned by photo service
 */
protocol NetworkPhotoServiceImage {
    /// Image network URL
    var imageURL: URL { get }
    /// Image size in pixels
    var imageSize: CGSize { get }
}
