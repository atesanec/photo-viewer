//
//  PhotoNetworkServicePhotoFeedItem.swift
//  PhotoViewer
//
//  Created by VI_Business on 01/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation

/**
 *  Photo service item returned by the feed
 */
protocol NetworkPhotoServicePhotoFeedItem {
    /// Unique id
    var photoId: String {get}
    /// Title
    var title: String {get}
    /// Fetch image with desired size
    func imageWithSize(preset: NetworkPhotoServiceImageSizePreset) -> NetworkPhotoServiceImage
}
