//
//  NetworkImageDownloader.swift
//  PhotoViewer
//
//  Created by VI_Business on 04/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

/**
 *  Network image downloader with built in cache
 */
class NetworkImageDownloader {
    /// Download image with target URL. Provides cached image immediately if available
    func downloadImage(url: URL) -> Observable<UIImage?> {
        return Observable<UIImage?>.create { (observer) -> Disposable in
            let resource = ImageResource(downloadURL: url)
            let task = KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil,
                                                              completionHandler: { (image, error, _, _) in
                                                                if let dowloadError = error {
                                                                    observer.onError(dowloadError)
                                                                } else {
                                                                    observer.onNext(image)
                                                                    observer.onCompleted()
                                                                }
            })
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
