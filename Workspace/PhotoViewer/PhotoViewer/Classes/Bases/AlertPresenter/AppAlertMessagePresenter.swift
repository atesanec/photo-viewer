//
//  AppAlertPresenter.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Class responsible for error message presentation within app
 */
class AppAlertMessagePresenter {
    func presentAlert(title: String? = nil, text: String? = nil) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel))
        UIViewController.topmostViewController.present(alert, animated: true, completion: nil)
    }
}
