//
//  RoundButton.swift
//  PhotoViewer
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Circle shaped button
 */
class RoundButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        layer.borderWidth = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = self.bounds.midX
    }
}
