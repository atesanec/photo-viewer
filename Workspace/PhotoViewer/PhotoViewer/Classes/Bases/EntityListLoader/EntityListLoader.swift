//
//  EntityListLoader.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift

/**
 *  Paged entity list loader to be used for UI scenarios
 */
class EntityListLoader<T> {
    /// All loaded entities
    let loadedEntities = BehaviorSubject<[T]>(value: [T]())
    /// Are all entities loaded
    let allLoaded = BehaviorSubject<Bool>(value: false)
    /// Whether list is being loaded now
    let isLoading = BehaviorSubject<Bool>(value: false)
    /// Loader configuration has changed. Usually that means it's a time to call loadNextBatch to repopulate the list
    let configurationChangedSignal = PublishSubject<Void>()
    
    private let loaderStrategy: EntityListLoaderStrategy<T>
    private let disposeBag = DisposeBag()
    private var pagerDisposeBag = DisposeBag()
    
    init(strategy: EntityListLoaderStrategy<T>) {
        self.loaderStrategy = strategy
        self.setupObservations()
        self.bindToPager()
    }
    
    /// Load next batch of items
    func loadNextBatch() {
        let isLoading = try! self.loaderStrategy.pager!.isLoading.value()
        let allLoaded = try! self.loaderStrategy.pager!.allLoaded.value()
        if !isLoading && !allLoaded {
            self.loaderStrategy.pager!.loadNext().subscribeOn(MainScheduler.instance)
                .subscribe(onNext: nil, onError: {e in
                    RootServicesDomain.sharedInstance.alertMessagePresenter.presentAlert(title: "DataLoadError".localized, text: nil)
                }).disposed(by: pagerDisposeBag)
        }
    }
    
    private func setupObservations() {
        loaderStrategy.pagerInvalidatedSignal.subscribe(onNext: { [weak self] _ in
            let strongSelf = self!
            strongSelf.bindToPager()
            strongSelf.configurationChangedSignal.onNext(())
        }).disposed(by: disposeBag)
    }
    
    private func bindToPager() {
        pagerDisposeBag = DisposeBag()
        loaderStrategy.pager.allLoaded.observeOn(MainScheduler.instance).bind(to: allLoaded).disposed(by: pagerDisposeBag)
        loaderStrategy.pager.isLoading.observeOn(MainScheduler.instance).bind(to: isLoading).disposed(by: pagerDisposeBag)
        loaderStrategy.pager.loadedEntities.observeOn(MainScheduler.instance).bind(to: loadedEntities).disposed(by: pagerDisposeBag)
    }
}
