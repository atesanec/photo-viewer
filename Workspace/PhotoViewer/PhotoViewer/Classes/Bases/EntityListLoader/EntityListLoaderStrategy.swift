//
//  EntityListLoaderStrategy.swift
//  PhotoViewer
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift

/**
 *  Strategy for entity list loader
 */
class EntityListLoaderStrategy<T> {
    /// Signals that current pager has been invalidated
    var pagerInvalidatedSignal = PublishSubject<Void>()
    
   /// Pager that loads entity list
   var pager: NetworkRequestOffsetPager<T>!
}
