//
//  CollectionViewSectionManagerAggregate.swift
//  PhotoViewer
//
//  Created by VI_Business on 02/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

/**
 *  Aggregate for collection view sections, playing the role of datasource and delegate for collectionview
 */
@objc class CollectionViewSectionManagerAggregate: NSObject, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private let sectionManagers: [CollectionViewSectionManager]
    private let disposeBag = DisposeBag()
    weak var collectionView: UICollectionView!
    
    init(sectionManagers: [CollectionViewSectionManager], collectionView: UICollectionView) {
        self.sectionManagers = sectionManagers.sorted(by: {$0.sectionIndex > $1.sectionIndex})
        
        self.collectionView = collectionView
        
        super.init()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        for manager in self.sectionManagers {
            manager.collectionView = collectionView
            manager.registerCells()
        }
        
        // Animate update whenever data changes
        Observable.combineLatest(sectionManagers.map({$0.dataObservable})).subscribe(onNext: { [weak self] _ in
            let strongSelf = self!
            strongSelf.collectionView.reloadData()
            strongSelf.collectionView.collectionViewLayout.invalidateLayout()
        }).disposed(by: disposeBag)
    }
    
    // MARK: UICollectionViewDataSource and UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return sectionManagers[indexPath.section].sizeForItem(atIndex: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return try! sectionManagers[section].dataObservable.value().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sectionManager = sectionManagers[indexPath.section]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: sectionManager.cellId(atIndex: indexPath.row), for: indexPath)
        sectionManager.configure(cell: cell, atIndex: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sectionManager = sectionManagers[indexPath.section]
        sectionManager.itemSelected(atIndex: indexPath.row)
    }
}
