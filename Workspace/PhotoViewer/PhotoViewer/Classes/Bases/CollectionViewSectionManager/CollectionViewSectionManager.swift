//
//  CollectionViewSectionManager.swift
//  PhotoViewer
//
//  Created by VI_Business on 02/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

/**
 *  Collection view section manager that handles all aspects relevant to a single collection view section
 */
protocol CollectionViewSectionManager: class {
    var collectionView: UICollectionView! { get set }
    /// Section index in collection view
    var sectionIndex: Int { get }
    /// Data related to this section
    var dataObservable: BehaviorSubject<[Any]> { get }
    
    /// Register section's cells here
    func registerCells()
    /// Configure cell with data
    func configure(cell: UICollectionViewCell, atIndex: Int)
    /// Reuse id for cell at index
    func cellId(atIndex: Int) -> String
    /// Size for cell at inded
    func sizeForItem(atIndex: Int) -> CGSize
    /// Cell selection handler
    func itemSelected(atIndex: Int)
}
