//
//  PhotoProcessor.swift
//  PhotoViewer
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

enum PhotoProcessorError: Error {
    case genericError
}

/**
 * Photo processor making various image transformations
 */
class PhotoProcessor {
    private static let frameWidthToImageWidthRatio: CGFloat = 1.0 / 6.0
    private static let textAreaHeightToImageHeightRatio: CGFloat = 1.0 / 4.0
    
    /// Draw image on a caption using "demotivator" style
    func makeCaptionedImage(image: UIImage, caption: String) -> Observable<UIImage> {
        return Observable<UIImage>.create { [weak self] (observer) -> Disposable in
            let strongSelf = self!
            
            if caption.isEmpty {
                observer.onNext(image)
                observer.onCompleted()
            } else {
                DispatchQueue.global().async {
                    guard let image = strongSelf.drawCaption(onImage: image, caption: caption) else {
                        observer.onError(PhotoProcessorError.genericError)
                        return
                    }
                    
                    observer.onNext(image)
                    observer.onCompleted()
                }
            }
            
            return Disposables.create()
        }
    }
    
    private func drawCaption(onImage image:UIImage, caption:String) -> UIImage? {
        let frameWidth = ceil(image.size.width * PhotoProcessor.frameWidthToImageWidthRatio)
        let textAreaSize = CGSize(width: image.size.width, height: ceil(image.size.height * PhotoProcessor.textAreaHeightToImageHeightRatio))
        let canvasSize = CGSize(width: image.size.width + 2 * frameWidth, height: 2 * frameWidth + image.size.height + textAreaSize.height)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 1.0)
        let context = UIGraphicsGetCurrentContext()!;
        
        let paragraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.alignment = .center
        let textFontAttributes = [
            NSAttributedStringKey.font: captionFont(withCaption: caption, forCaptionBounds: textAreaSize),
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.paragraphStyle: paragraphStyle
            ]
        
        UIColor.black.setFill()
        context.fill(CGRect(origin: CGPoint(), size: canvasSize))
        
        image.draw(in: CGRect(x: frameWidth, y: frameWidth, width: image.size.width, height: image.size.height))
        caption.draw(with: CGRect(x: frameWidth,
                                  y: canvasSize.height - frameWidth - textAreaSize.height,
                                  width: textAreaSize.width,
                                  height: textAreaSize.height),
                     options: [.usesLineFragmentOrigin],
                     attributes: textFontAttributes,
                     context: nil)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return newImage
    }
    
    private func captionFont(withCaption: String, forCaptionBounds: CGSize) -> UIFont {
        var fontSize: CGFloat = 0.0
        var captionHeight: CGFloat = 0.0
        while captionHeight <= forCaptionBounds.height {
            fontSize += 1
            let font = UIFont.systemFont(ofSize: fontSize)
            captionHeight = withCaption.heightWithConstrainedWidth(width: forCaptionBounds.width, font: font)
        }
        
        return UIFont.systemFont(ofSize: fontSize - 1)
    }
}
